import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

// method that build the application using App.vue as the base and index.js as the router
createApp(App).use(router).mount('#app')
