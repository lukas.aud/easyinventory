import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Inventory from '../views/Inventory.vue'
import Register from '../components/Register.vue'
import Profile from '../views/Profile.vue'
import updateItem from '../components/updateItem.vue'
import updateInventory from '../components/updateInventory.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    // Pathing to the inventory page from the navigation buttons
    path: '/inventory/:inventory_id',
    name: 'Inventory',
    component: Inventory
    // Previous component path
    // component: () => import(/* webpackChunkName: "inventory" */ '../views/Inventory.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/profile/:user_id',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/inventory/:inventory_id/edit',
    name: 'updateItem',
    component: updateItem
  },
  {
    path: '/profile/:user_id/edit',
    name: updateInventory,
    component: updateInventory
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
